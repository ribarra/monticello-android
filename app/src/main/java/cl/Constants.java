package cl;

public class Constants {

    public final static String EVENT_ERROR_LOGIN = "error-login";
    public final static String EVENT_ERROR_LOGIN_CODE = "code";

    public final static String EVENT_TIME_LOGIN = "time-login";
    public final static String EVENT_TIME_SECONDS = "seconds";

    public final static int PLAYTECH_ACTIVITY_REQUEST = 1000;
}
