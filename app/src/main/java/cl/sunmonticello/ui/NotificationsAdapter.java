package cl.sunmonticello.ui;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Locale;

import cl.sunmonticello.R;
import cl.sunmonticello.db.models.MyNotification;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by ribarra on 12/19/16.
 */

public class NotificationsAdapter extends ArrayAdapter<MyNotification> {

    public NotificationsAdapter(Context context) {

        super(context, 0);

        RealmResults<MyNotification> results = Realm.getDefaultInstance().where(MyNotification.class).findAllSorted("createdAt", Sort.DESCENDING);
        addAll(results);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            //We must create a View:
            convertView = ((LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_notification, parent, false);
        }


        Log.d("TAG", "-- position. " + position);

        final MyNotification notification = getItem(position);
        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                notification.setRead(true);
            }
        });

        ImageView imageView = (ImageView) convertView.findViewById(R.id.notification_image);
        TextView textViewTitle = (TextView) convertView.findViewById(R.id.notification_title);
        TextView textViewDescription = (TextView) convertView.findViewById(R.id.notification_description);
        TextView textViewTimeAgo = (TextView) convertView.findViewById(R.id.notification_time_ago);

        Picasso.with(parent.getContext()).load(notification.getImage()).into(imageView);

        textViewTitle.setText(notification.getTitle());
        textViewDescription.setText(notification.getDescription());

        Locale.setDefault(new Locale("es", "ES"));
        textViewTimeAgo.setText(DateUtils.getRelativeTimeSpanString(notification.getCreatedAt(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));

        //Here we can do changes to the convertView, such as set a text on a TextView
        //or an image on an ImageView.
        return convertView;
    }
}
