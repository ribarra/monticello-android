package cl.sunmonticello.ui;

import android.content.Context;
import android.support.v4.util.Pair;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import cl.sunmonticello.MySession;
import cl.sunmonticello.R;


/**
 * Created by ribarra on 26/12/16.
 */

public class MenuAdapter extends ArrayAdapter<Pair<String, Integer>> {

    public static final int ROW_MVG = 10;
    public static final int ROW_SUNREWARDS_POINTS = 11;
    public static final int ROW_FAVORITES = 20;

    public static final int ROW_PLAYTECH = 30;

    public MenuAdapter(Context context) {
        super(context, 0);

        add(new Pair<>("INICIO", 0));
        add(new Pair<>("HOY", 7));
        add(new Pair<>("FAVORITOS", ROW_FAVORITES));
        add(new Pair<>("GRANDES EVENTOS", 2));
        //add(new Pair<>("CARTELERA BRAVO BAR", 3));
        add(new Pair<>("SORTEOS Y PROMOCIONES", 4));
        add(new Pair<>("SUKA CLUB", 5));
        add(new Pair<>("GASTRONOMÍA", 6));
        if(MySession.get().getUser().isSun()) {
            add(new Pair<>("PUNTOS SUN REWARDS", ROW_SUNREWARDS_POINTS));
        }
        else
        {
            add(new Pair<>("SUN REWARDS", ROW_MVG));
        }

        add(new Pair<>("JUEGA EN LÍNEA", ROW_PLAYTECH));

        notifyDataSetChanged();
    }

    public int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.row_menu, null);
        }

        if(lastPosition == position)
        {
            view.setBackgroundResource(R.drawable.bg_menu_row_active);
        }
        else
        {
            view.setBackgroundResource(R.drawable.bg_menu_row);
        }

        TextView textViewTitle = view.findViewById(R.id.menu_title);
        textViewTitle.setText(getItem(position).first);

        return view;
    }
}
