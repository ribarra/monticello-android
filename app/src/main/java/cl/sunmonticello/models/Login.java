package cl.sunmonticello.models;

import com.google.gson.annotations.SerializedName;

public class Login {

    public Login(String rut, String clave)
    {
        this.rut = rut;
        this.clave = clave;
    }

    public String rut;

    public String clave;
}
