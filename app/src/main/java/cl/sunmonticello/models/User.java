package cl.sunmonticello.models;

import com.google.gson.annotations.SerializedName;

public class User implements IUser {

    public static final String METHOD = "login-universal";

    @SerializedName("casino_usuario_id")
    public String id;

    @SerializedName("rut")
    public String rut;

    @SerializedName("fecha_nacimiento")
    public String birthday;

    @SerializedName("nombre")
    public String first_name;

    @SerializedName("apellido")
    public String last_name;

    public String email;

    @SerializedName("en_club")
    public boolean sun;

    @SerializedName("puntos")
    public String points;

    @SerializedName("expiry_date")
    public String expiryDate;

    @SerializedName("cuenta_verificada")
    public boolean verified;

    @Override
    public String toString()
    {
        return "rut. " + rut;
    }

    @Override
    public String getID() {
        return email;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getName() {
        return first_name + " " + last_name;
    }

    @Override
    public String getMethod() {
        return METHOD;
    }

    @Override
    public boolean isGuest() {
        return false;
    }

    @Override
    public boolean isSun() {
        return sun;
    }

    public int getPoints()
    {
        return (int) Float.parseFloat(points);
    }

    public String getExpiryDate()
    {
        return expiryDate;
    }
}
