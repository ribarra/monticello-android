package cl.sunmonticello.models;

public interface IUser {

    String getID();

    String getEmail();
    String getName();
    String getMethod();

    boolean isGuest();

    boolean isSun();
}
