package cl.sunmonticello.models;

import java.util.UUID;

public class Guest implements IUser {

    @Override
    public String getID() {
        return UUID.randomUUID().toString();
    }

    @Override
    public String getEmail() {
        return "guest@guest.cl";
    }

    @Override
    public String getName() {
        return "Invitado";
    }

    @Override
    public String getMethod() {
        return "guest";
    }

    @Override
    public boolean isSun() {
        return false;
    }

    @Override
    public boolean isGuest() {
        return true;
    }

}
