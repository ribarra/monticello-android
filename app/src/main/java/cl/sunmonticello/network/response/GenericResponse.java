package cl.sunmonticello.network.response;

import com.google.gson.annotations.SerializedName;

public class GenericResponse {

    @SerializedName("respuesta")
    public Response response;

}
