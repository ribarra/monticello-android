package cl.sunmonticello.network.response;

import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("codigo")
    public int code;

    @SerializedName("descripcion")
    public String description;

    public String toString()
    {
        return code + " - " + description;
    }
}
