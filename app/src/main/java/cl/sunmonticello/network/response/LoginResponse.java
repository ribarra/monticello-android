package cl.sunmonticello.network.response;

import com.google.gson.annotations.SerializedName;

import cl.sunmonticello.models.User;

public class LoginResponse {

    @SerializedName("respuesta")
    public Response response;

    @SerializedName("usuario")
    public User user;

    public String toString()
    {
        return response + " - " + user;
    }
}
