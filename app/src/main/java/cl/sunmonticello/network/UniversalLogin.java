package cl.sunmonticello.network;

import cl.sunmonticello.network.response.GenericResponse;
import cl.sunmonticello.network.response.LoginResponse;
import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UniversalLogin {

    @POST("iniciar-sesion.php")
    @FormUrlEncoded
    Single<LoginResponse> login(@Field("rut") String rut, @Field("clave") String password);

    @POST("consultar-existencia-por-rut.php")
    @FormUrlEncoded
    Single<LoginResponse> check(
            @Field("rut") String rut);

    @POST("crear-nueva-cuenta.php")
    @FormUrlEncoded
    Single<GenericResponse> signUp(
            @Field("rut") String rut,
            @Field("nombre") String first_name,
            @Field("apellido") String last_name,
            @Field("email") String email,
            @Field("fecha_nacimiento") String birthday,
            @Field("clave") String password);

    @POST("enviar-enlace-recuperacion-contrasena.php")
    @FormUrlEncoded
    Single<GenericResponse> forgotPassword(
            @Field("rut") String rut);
}
