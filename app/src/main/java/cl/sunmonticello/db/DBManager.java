package cl.sunmonticello.db;

import android.content.Context;

import io.realm.Realm;

/**
 * Created by ribarra on 12/12/16.
 */

public class DBManager {

    private static DBManager INSTANCE;
    private static Realm REALM_INSTANCE;

    public static void setup(Context context)
    {
        if(INSTANCE == null)
            REALM_INSTANCE = Realm.getDefaultInstance();

    }

    public static DBManager getInstance()
    {
        return INSTANCE;
    }
}
