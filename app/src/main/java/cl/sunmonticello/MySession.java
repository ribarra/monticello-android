package cl.sunmonticello;

import android.content.Context;
import android.content.SharedPreferences;

import cl.sunmonticello.models.IUser;

/**
 * Created by ribarra on 09/03/17.
 */

public class MySession {

    private static final MySession INSTANCE = new MySession();

    private IUser user;

    public static MySession get()
    {
        return INSTANCE;
    }

    public IUser getUser()
    {
        return user;
    }

    public void setup(IUser user)
    {
        this.user = user;
    }

    public void save(Context context, String rut, String password)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(Config.PACKAGE_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(Config.KEY_RUT, rut);
        editor.putString(Config.KEY_PASSWORD, password);
        editor.commit();
    }

    public void clear(Context context)
    {
        //set user to null
        user = null;

        // remove data saved on preferences
        SharedPreferences.Editor editor = context.getSharedPreferences(Config.PACKAGE_NAME, Context.MODE_PRIVATE).edit();
        editor.remove(Config.KEY_RUT);
        editor.remove(Config.KEY_PASSWORD);
        editor.commit();
    }

}
