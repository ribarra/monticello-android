package cl.sunmonticello.external;

import android.webkit.JavascriptInterface;

/**
 * Created by ribarra on 28/12/16.
 */

public interface WebAppInterface {

    @JavascriptInterface
    void EnterFS();

    @JavascriptInterface
    void ExitFS();

    @JavascriptInterface
    void Logout();

    @JavascriptInterface
    void hideBar();

    @JavascriptInterface
    void showBar();

    @JavascriptInterface
    void AppShare(String url);

}
