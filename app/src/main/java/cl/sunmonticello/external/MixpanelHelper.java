package cl.sunmonticello.external;

import android.content.Context;

import com.google.common.collect.ImmutableMap;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.util.HashMap;
import java.util.Map;

import cl.sunmonticello.models.IUser;

/**
 * Created by ribarra on 03/01/17.
 */

public class MixpanelHelper {

    private static MixpanelAPI MIXPANEL;
    private final static MixpanelHelper INSTANCE = new MixpanelHelper();

    private MixpanelHelper()
    {

    }

    public static MixpanelHelper get()
    {
        return INSTANCE;
    }

    public void setup(Context context, String token)
    {
        MIXPANEL = MixpanelAPI.getInstance(context, token);
    }

    public void identify(IUser user)
    {
        MIXPANEL.getPeople().identify(user.getID());

        registerSuperProperties(ImmutableMap.of(
                "uid", user.getID(),
                "email", user.getEmail(),
                "name", user.getName(),
                "provider", user.getMethod()));
    }

    public void registerSuperProperties(Map<String, String> props)
    {
        Map<String, Object> myProps = new HashMap<>();
        for(String key : props.keySet())
        {
            myProps.put(key, props.get(key));
        }
        MIXPANEL.registerSuperPropertiesMap(myProps);
    }

    public void timeEvent(String event)
    {
        MIXPANEL.timeEvent(event);
    }

    public void track(String event)
    {
        MIXPANEL.track(event);
    }

    public void track(String event, Map<String, ? extends Object> props)
    {
        Map<String, Object> myProps = new HashMap<>();
        for(String key : props.keySet())
        {
            myProps.put(key, props.get(key));
        }
        MIXPANEL.trackMap(event, myProps);
    }
}
