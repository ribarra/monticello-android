package cl.sunmonticello;

/**
 * Created by ribarra on 03/01/17.
 */

public class Config {

    public static final String HOST = "http://70.32.73.73/mobile";
    public static final String API = "https://sunmonticello.cl/mobile/api/";
    public static final String PLAYTECH_URL = "https://games.playtechone.com/wpl3/live_desktop/?username=liveuser200&password=123asdASD&launch_alias=rol_monticello";

    public static final String MIXPANEL_TOKEN = "d07e4a78894f5012cff137779bf1cfd9";

    public static final String PACKAGE_NAME = "cl.sunmonticello";
    public static final String KEY_OPENED_FROM_NOTIFICATION = "openedFromNotification";

    public static final String KEY_RUT = "rut";
    public static final String KEY_PASSWORD = "password";
}
