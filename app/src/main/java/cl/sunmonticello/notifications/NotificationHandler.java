package cl.sunmonticello.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import cl.sunmonticello.R;
import cl.sunmonticello.activities.MainActivity;
import cl.sunmonticello.db.models.MyNotification;
import io.realm.Realm;

/**
 * Created by ribarra on 12/26/16.
 */

public class NotificationHandler extends NotificationExtenderService {
    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {

        Realm realm = Realm.getDefaultInstance();


        try {

            Log.d("TAG", "-- payload. " + receivedResult.payload.additionalData.toString());


            JSONObject data = receivedResult.payload.additionalData;
            final int postID = data.getInt("postID");
            final String title = data.getString("title");
            final String image = data.getString("image");
            // final String encodedImage = data.getString("encoded-image");

            // Use them like regular java objects
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    MyNotification myNotification = realm.createObject(MyNotification.class);
                    myNotification.setCreatedAt(System.currentTimeMillis());
                    myNotification.setPostID(postID);
                    myNotification.setTitle(title);
                    myNotification.setImage(image);
                }
            });

            Intent intent = new Intent();
            intent.setAction("cl.magnesia.monticello.notifications");
            sendBroadcast(intent);

            // Fire off the notification

            new SendNotification(getApplicationContext()).execute(title, image);

            /*
            byte[] decodedImage = Base64.decode(encodedImage, Base64.DEFAULT);

            //Bitmap bitmap = BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length);

            NotificationManager nm =
                    (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            // Pick an id that probably won't overlap anything
            int notificationId = (int) System.currentTimeMillis();

            Notification notification = null;
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
            builder.setContentTitle("Monticello")
                    .setContentText(title)
                    //.setTicker("tickettext")
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setLargeIcon(bitmap)
                    //.setSmallIcon(this.getSmallIconId(context, intent))
                    //.setLargeIcon(this.getLargeIcon(context, intent))
                    //.setContentIntent(pContentIntent)
                    //.setDeleteIntent(pDeleteIntent)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL);

            notification = builder.build();


            nm.notify(notificationId, notification);
*/

        } catch (JSONException exception) {
            exception.printStackTrace();
        }

        /*
         catch (SecurityException e) {
            // Some phones throw an exception for unapproved vibration
            if (notification != null) {
                notification.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND;
                nm.notify(notificationId, notification);
            }
        }
         */


        // Return true to stop the notification from displaying.
        return true;
    }

    private class SendNotification extends AsyncTask<String, Void, Bitmap> {

        Context context;
        String title;

        public SendNotification(Context context) {
            super();
            this.context = context;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            InputStream in;
            title = params[0];
            try {

                URL url = new URL(params[1]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                return myBitmap;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {

            super.onPostExecute(result);
            try {



                int id = (int) System.currentTimeMillis();
                NotificationManager notificationManager = (NotificationManager) context
                        .getSystemService(Context.NOTIFICATION_SERVICE);

                //Intent intent = new Intent(ctx, NotificationsActivity.class);
                //intent.putExtra("isFromBadge", false);


                Notification notification = new Notification.Builder(context)
                        .setContentTitle(
                                context.getResources().getString(R.string.app_name))
                        .setContentText(title)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setLargeIcon(result)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .build();

                Intent resultIntent = new Intent(context, MainActivity.class);
                PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                notification.contentIntent = resultPendingIntent;


                // hide the notification after its selected
                notification.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(id, notification);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}