package cl.sunmonticello.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;

import org.json.JSONException;
import org.json.JSONObject;

import cl.sunmonticello.R;
import cl.sunmonticello.activities.MainActivity;
import cl.sunmonticello.db.models.MyNotification;
import io.realm.Realm;

/**
 * Created by ribarra on 12/26/16.
 */

public class SimpleNotificationHandler extends NotificationExtenderService {
    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {

        Realm realm = Realm.getDefaultInstance();


        try {

            Log.d("TAG", "-- payload. " + receivedResult.payload.additionalData.toString());


            JSONObject data = receivedResult.payload.additionalData;
            final int postID = data.getInt("postID");
            final String title = data.getString("title");
            final String image = data.getString("image");
            // final String encodedImage = data.getString("encoded-image");

            // Use them like regular java objects
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    MyNotification myNotification = realm.createObject(MyNotification.class);
                    myNotification.setCreatedAt(System.currentTimeMillis());
                    myNotification.setPostID(postID);
                    myNotification.setTitle(title);
                    myNotification.setImage(image);
                }
            });

            Intent intent = new Intent();
            intent.setAction("cl.magnesia.monticello.notifications");
            sendBroadcast(intent);

            // Fire off the notification

            try {



                int id = (int) System.currentTimeMillis();
                NotificationManager notificationManager = (NotificationManager) getApplicationContext()
                        .getSystemService(Context.NOTIFICATION_SERVICE);

                //Intent intent = new Intent(ctx, NotificationsActivity.class);
                //intent.putExtra("isFromBadge", false);


                Notification notification = new Notification.Builder(getApplicationContext())
                        .setContentTitle(
                                getApplicationContext().getResources().getString(R.string.app_name))
                        .setContentText(title)
                        .setSmallIcon(R.drawable.ic_stat_name)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.icon_notification))
                        .setDefaults(Notification.DEFAULT_ALL)
                        .build();

                Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                notification.contentIntent = resultPendingIntent;


                // hide the notification after its selected
                notification.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(id, notification);


            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (JSONException exception) {
            exception.printStackTrace();
        }

        /*
         catch (SecurityException e) {
            // Some phones throw an exception for unapproved vibration
            if (notification != null) {
                notification.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND;
                nm.notify(notificationId, notification);
            }
        }
         */


        // Return true to stop the notification from displaying.
        return true;
    }

}