package cl.sunmonticello.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.net.UrlQuerySanitizer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.VideoView;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cl.sunmonticello.Config;
import cl.sunmonticello.MySession;
import cl.sunmonticello.R;
import cl.sunmonticello.external.MixpanelHelper;
import cl.sunmonticello.models.Guest;
import cl.sunmonticello.models.IUser;
import cl.sunmonticello.models.User;
import cl.sunmonticello.network.UniversalLogin;
import io.fabric.sdk.android.services.common.Crash;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static cl.Constants.EVENT_ERROR_LOGIN;
import static cl.Constants.EVENT_ERROR_LOGIN_CODE;
import static cl.Constants.EVENT_TIME_LOGIN;
import static cl.Constants.EVENT_TIME_SECONDS;

public class WelcomeActivity extends AppCompatActivity {

    @BindView(R.id.web_view)
    WebView webView;

    @BindView(R.id.welcome_videoview)
    VideoView videoView;

    /* OFFLINE */
    @BindView(R.id.layout_offline)
    View layoutOffline;

    UniversalLogin service;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        ButterKnife.bind(this);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "cl.sunmonticello",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException exception) {
            exception.printStackTrace();
        } catch (NoSuchAlgorithmException exception) {
            exception.printStackTrace();
        }

        // setup retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(Config.API)
                .build();
        service = retrofit.create(UniversalLogin.class);

        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(
                new WebViewClient() {

                    private boolean success = true;

                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        success = true;
                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        // do your stuff here
                        Log.d("TAG", "-- page ready. " + url + ", success: " + success);

                        if (success) {
                            webView.setVisibility(View.VISIBLE);
                            layoutOffline.setVisibility(View.GONE);
                            callJS("logo_ready()");
                            new Handler().postDelayed(() -> callJS("login_ready()"), 3000);
                        } else {
                            onOffline();
                        }

                    }

                    @Override
                    public void onReceivedError(WebView view, int errorCode,
                                                String description, String failingUrl) {
                        success = false;
                        Log.d("WEBVIEW", "ON PAGE error");
                    }

                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        return handleURL(url);
                    }
                }

        );
        webView.addJavascriptInterface(new Object()
        {
            @JavascriptInterface
            public void Guest() {

                IUser user = new Guest();
                MySession.get().setup(user);
                doIntent(user);
            }
        }, "Android");

        String path = "android.resource://cl.sunmonticello/raw/video";
        videoView.setVideoURI(Uri.parse(path));
        videoView.setOnPreparedListener(mediaPlayer -> mediaPlayer.setLooping(true));
        videoView.start();

        load();

        // check if is there any previous session
        checkSession();

    }

    private void checkSession()
    {
        SharedPreferences preferences = getSharedPreferences(Config.PACKAGE_NAME, Context.MODE_PRIVATE);
        String rut = preferences.getString(Config.KEY_RUT, null);
        String password = preferences.getString(Config.KEY_PASSWORD, null);

        if(rut != null && password != null)
        {
            // there is a session, try to login
            tryLogin(rut, password, true);
        }
    }

    @OnClick(R.id.layout_offline)
    public void onClickOffline() {
        layoutOffline.setVisibility(View.GONE);
        load();
    }

    private void onOffline() {
        webView.setVisibility(View.GONE);
        layoutOffline.setVisibility(View.VISIBLE);

        MixpanelHelper.get().track("Welcome offline");
    }

    private void callJS(String code) {
        Log.d("TAG", "--js. " + code);
        webView.loadUrl("javascript:" + code);
    }

    private void load() {

        webView.loadUrl(String.format("%s/login-v2/", Config.HOST));

    }

    private boolean handleURL(String string) {

        Log.d("TAG", "-- url. " + string);
        try {
            URL url = new URL(string);

            Log.d("TAG", "-- host. " + url.getHost());
            Log.d("TAG", "-- path. " + url.getPath());
            Log.d("TAG", "-- query. " + url.getQuery());

            if (url.getHost().equals("monticello.mobile")) {

                UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(string);
                Map<String, String> params = new HashMap<>();

                for (UrlQuerySanitizer.ParameterValuePair pair : sanitizer.getParameterList())
                    if (pair.mValue != null)
                        params.put(pair.mParameter, pair.mValue);

                if (url.getPath().equals("/login")) {

                    String rut = params.get("rut");
                    String password = params.get("password");

                    login(rut, password);
                    return true;
                } else if (url.getPath().equals("/check")) {

                    Log.d("TAG", "-- path: /check");

                    String rut = params.get("rut");
                    check(rut);

                    return true;
                } else if (url.getPath().equals("/set-password")) {

                    String password = params.get("password");
                    signUp(user.rut, user.first_name, user.last_name, user.email, user.birthday, password);
                    return true;
                }
                else if (url.getPath().equals("/forgot")) {

                    Log.d("TAG", "-- path: /forgot");

                    String rut = params.get("rut");
                    forgot(rut);
                    return true;
                }

            } else if (url.getPath().endsWith(".pdf")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(string));
                startActivity(intent);
                return true;
            }


            return false;

        } catch (MalformedURLException exception) {
            exception.printStackTrace();
        }
        return false;

    }

    private void check(String rut)
    {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Cargando...");
        service.check(rut)
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(() -> dialog.dismiss())
                .subscribeOn(Schedulers.io())
                .subscribe(response -> {

                    Log.d("TAG", "user. " + response.user);
                    if(response.user == null)
                    {
                        // no user
                        callJS("PasswordNoUser()");
                    }
                    else
                    {
                        Log.d("TAG", "verified. " + response.user.verified);
                        if(response.user.verified)
                        {
                            callJS("PasswordAlreadyExists()");
                        }
                        else
                        {
                            user = response.user;
                            callJS(String.format("PasswordUser(\"%s\")", response.user.email));
                        }
                    }

                }, throwable -> {

                    Crashlytics.logException(throwable);

                });
    }

    private void tryLogin(String rut, String password, boolean silence)
    {

        final long t0 = System.currentTimeMillis();

        final ProgressDialog dialog = ProgressDialog.show(WelcomeActivity.this, "", "Cargando...");
        service.login(rut, password)
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> {

                    // close dialog
                    dialog.dismiss();

                    // log timing on answers
                    float seconds = (System.currentTimeMillis() - t0) * 0.001f;
                    CustomEvent event = new CustomEvent(EVENT_TIME_LOGIN);
                    event.putCustomAttribute(EVENT_TIME_SECONDS, seconds);
                    Answers.getInstance().logCustom(event);

                })
                .subscribeOn(Schedulers.io())
                .subscribe(response -> {
                    if (response.response.code > 0) {
                        if(!silence)
                        {
                            error(response.response.description);

                            LoginEvent event = new LoginEvent()
                                    .putMethod(User.METHOD)
                                    .putSuccess(false)
                                    .putCustomAttribute(EVENT_ERROR_LOGIN_CODE, Integer.toString(response.response.code));
                            Answers.getInstance().logLogin(event);

                        }
                    } else {
                        if(!silence)
                        {
                            MySession.get().save(WelcomeActivity.this, rut, password);
                        }

                        // setup user instance
                        MySession.get().setup(response.user);
                        doIntent(response.user);
                    }

                }, throwable -> {

                    Crashlytics.logException(throwable);

                });
    }

    private void doIntent(IUser user) {

        // identify on mixpanel
        MixpanelHelper.get().identify(user);

        // identify on crashlytics
        Crashlytics.setUserIdentifier(user.getID());
        Crashlytics.setUserEmail(user.getEmail());

        // send data to crashlytics
        LoginEvent event = new LoginEvent().putMethod(user.getMethod()).putSuccess(true);
        Answers.getInstance().logLogin(event);

        // do intent
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void login(String rut, String password) {

        if (rut.isEmpty() || password.isEmpty()) {
            error("Debe ingresar rut y password");
            return;
        }

        tryLogin(rut, password, false);
    }

    private void signUp(
            String rut,
            String first_name,
            String last_name,
            String email,
            String birthday,
            String password) {

        final ProgressDialog dialog = ProgressDialog.show(this, "", "Cargando...");

        service.signUp(rut, first_name, last_name, email, birthday, password)
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(() -> dialog.dismiss())
                .subscribeOn(Schedulers.io())
                .subscribe(response -> {
                    if (response.response.code > 0) {

                        // TODO error
                        error(response.response.description);
                        CustomEvent event = new CustomEvent(EVENT_ERROR_LOGIN);
                        event.putCustomAttribute(EVENT_ERROR_LOGIN_CODE, response.response.code);
                        Answers.getInstance().logCustom(event);

                    } else {

                        // TODO show toast
                        callJS("SignUpCallback()");
                    }

                }, throwable -> {

                    Log.d("TAG", "-- error");
                    throwable.printStackTrace();

                });

    }

    private void forgot(String rut) {

        final ProgressDialog dialog = ProgressDialog.show(this, "", "Cargando...");

        service.forgotPassword(rut)
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate(() -> dialog.dismiss())
                .subscribeOn(Schedulers.io())
                .subscribe(response -> {

                    // TODO: anything to handle?

                }, throwable -> {

                    Crashlytics.getInstance().logException(throwable);

                });

    }

    private void error(String message) {
        String js = String.format("login_error([\"%s\"])", message);
        callJS(js);
    }
}
