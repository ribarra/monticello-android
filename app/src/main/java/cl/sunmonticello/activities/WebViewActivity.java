package cl.sunmonticello.activities;

/**
 * Created by ribarra on 04/01/17.
 */

public interface WebViewActivity {

    void callJS(String javascript);
}
