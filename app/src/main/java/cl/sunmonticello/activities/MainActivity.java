package cl.sunmonticello.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.net.UrlQuerySanitizer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cl.Constants;
import cl.sunmonticello.Config;
import cl.sunmonticello.MonticelloApplication;
import cl.sunmonticello.MySession;
import cl.sunmonticello.R;
import cl.sunmonticello.db.models.MyNotification;
import cl.sunmonticello.external.WebAppInterface;
import cl.sunmonticello.models.User;
import cl.sunmonticello.navigation.NavigationStack;
import cl.sunmonticello.navigation.commands.Calendar;
import cl.sunmonticello.navigation.commands.Command;
import cl.sunmonticello.navigation.commands.Home;
import cl.sunmonticello.navigation.commands.HomeFavorites;
import cl.sunmonticello.navigation.commands.MVG;
import cl.sunmonticello.navigation.commands.Search;
import cl.sunmonticello.navigation.commands.SunRewardsPoints;
import cl.sunmonticello.ui.MenuAdapter;
import cl.sunmonticello.ui.NestedWebView;
import cl.sunmonticello.ui.NotificationsAdapter;
import io.realm.Realm;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks, WebViewActivity {

    public static final int PERMISSION_ACCESS_FINE_LOCATION = 1000;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.bg_mvg)
    ImageView ImageViewMVG;

    @BindView(R.id.search)
    EditText editTextSearch;

    @BindView(R.id.listview_menu)
    ListView listViewMenu;

    @BindView(R.id.webview)
    public NestedWebView webView;

    public ImageButton buttonCalendar;

    /* NOTIFICATIONS */
    @BindView(R.id.notifications)
    View viewNotifications;

    @BindView(R.id.notifications_wrapper)
    View viewNotificationsWrapper;

    @BindView(R.id.text_view_notifications)
    TextView textViewNotifications;

    @BindView(R.id.listview)
    public ListView listView;

    @BindView(R.id.notifications_empty)
    TextView textViewNotificationsEmpty;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        // setup onesignal
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler())
                .setNotificationReceivedHandler(new MyNotificationReceivedHandler()).init();

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TypedValue tv = new TypedValue();
        int height = 0;
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            height = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        final int appBarHeight = height;

        TextView textViewName = findViewById(R.id.menu_name);
        textViewName.setText(String.format("Hola! %s", MySession.get().getUser().getName()));

        //swipeRefreshLayout.setOnRefreshListener(this);

        webView.getSettings().setJavaScriptEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)

        {
            // chromium, enable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else

        {
            // older android version, disable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(
                new WebViewClient() {

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        // do your stuff here
                        Log.d("TAG", "-- page ready. " + url);
                        // callJS(String.format("SetUserID(\"%s\")", FirebaseAuth.getInstance().getCurrentUser().getUid()));
                        didFinish = true;

                        loadCategory(-1);
                    }

                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String urlString) {
                        Log.d("TAG", "-- url. " + urlString);
                        try {
                            URL url = new URL(urlString);

                            if (url.getHost().equals("monticello.mobile")) {
                                UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(urlString);
                                Log.d("TAG", "-- parameter: " + sanitizer.getParameterList().size());
                                Map<String, String> params = new HashMap<>();

                                for (UrlQuerySanitizer.ParameterValuePair pair : sanitizer.getParameterList())
                                {
                                    if (pair.mValue != null)
                                        params.put(pair.mParameter, pair.mValue);
                                }

                                if (url.getPath().equals("/mvg")) {
                                    return true;
                                } else if (url.getPath().equals("/profile")) {
                                    return true;
                                }
                                } else if (url.getPath().endsWith(".pdf")) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
                                    startActivity(intent);
                                    return true;
                                } else if (Config.HOST.contains(url.getHost()))
                                {
                                    return false;
                                } else {
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setData(Uri.parse(urlString));
                                    startActivity(intent);
                                    return true;
                            }

                        } catch (MalformedURLException exception) {
                            exception.printStackTrace();
                        }
                        return false;
                        }
                });

        webView.addJavascriptInterface(
                new WebAppInterface() {

                    @JavascriptInterface
                    public void Logout() {
                        AppLogout();
                    }

                    @JavascriptInterface
                    public void AppProfile() {
                        profile();
                    }

                    @JavascriptInterface
                    @Override
                    public void hideBar() {

                    }

                    @JavascriptInterface
                    @Override
                    public void EnterFS() {

                        runOnUiThread(() -> {

                            appBarLayout.setExpanded(false, true);

                            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
                            layoutParams.height = 0;
                            appBarLayout.setLayoutParams(layoutParams);

                        });

                    }

                    @JavascriptInterface
                    @Override
                    public void ExitFS() {

                        Log.d("TAG", "-- Exit FS");
                        runOnUiThread(() -> {

                            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
                            layoutParams.height = appBarHeight;
                            appBarLayout.setLayoutParams(layoutParams);

                            appBarLayout.setExpanded(true, true);

                        });


                    }


                    @JavascriptInterface
                    @Override
                    public void showBar() {
                        Log.d("TAG", "--  showBar");
                        //topbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));

                    }

                    @JavascriptInterface
                    @Override
                    public void AppShare(String url) {
                        Log.d("TAG", "--share. " + url);

                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "Compartir");
                        i.putExtra(Intent.EXTRA_TEXT, url);
                        startActivity(Intent.createChooser(i, "Compartir"));
                    }
                }, "Android");

        drawer = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)

        {


            Log.d("TAG", "-- check");
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                Log.d("TAG", "-- should show");

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                Log.d("TAG", "-- should show not");

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_ACCESS_FINE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else

        {

            Log.d("TAG", "-- permission OK");

            MonticelloApplication application = (MonticelloApplication) getApplication();
            application.getAPIClient().registerConnectionCallbacks(this);

        }

        IntentFilter intentFilter = new IntentFilter("cl.magnesia.monticello.notifications");

        registerReceiver(new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateNotificationsUI();

            }
            }, intentFilter);

        IntentFilter intentFilterNotifications = new IntentFilter(Config.KEY_OPENED_FROM_NOTIFICATION);

        registerReceiver(new BroadcastReceiver() {
                             @Override
                             public void onReceive(Context context, Intent intent) {

                                 if (!didFinish)
                                     return;


                                 SharedPreferences preferences = getSharedPreferences(Config.PACKAGE_NAME, Context.MODE_PRIVATE);
                                 int postID = preferences.getInt(Config.KEY_OPENED_FROM_NOTIFICATION, 0);

                                 Log.d("TAG", "-- received. " + didFinish + ", " + postID);

                                 if (postID > 0) {
                                     callJS(String.format("load_event(%d, \"%s\")", postID, ""));
                                 }

                                 SharedPreferences.Editor editor = getSharedPreferences(Config.PACKAGE_NAME, Context.MODE_PRIVATE).edit();
                                 editor.remove(Config.KEY_OPENED_FROM_NOTIFICATION);
                                 editor.commit();


                             }
                         }

                , intentFilterNotifications);

        // search
        editTextSearch.setOnEditorActionListener((textView, actionId, event) -> {

            if (event == null) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String query = editTextSearch.getText().toString();
                    Search(query);
                    return true;
                }
            }
            return false;
        });

        // menu
        final MenuAdapter adapter = new MenuAdapter(this);
        listViewMenu.setAdapter(adapter);
        listViewMenu.setOnItemClickListener((adapterView, view, position, l) -> {

                    adapter.lastPosition = position;

                    if (adapterView.getSelectedView() != null) {
                        adapterView.getSelectedView().setBackgroundResource(R.drawable.bg_menu_row);
                    }

                    Pair<String, Integer> pair = (Pair<String, Integer>) adapterView.getItemAtPosition(position);

                    if (pair.second == MenuAdapter.ROW_FAVORITES) {
                        navigate(new HomeFavorites(MainActivity.this));
                    } else if (pair.second == MenuAdapter.ROW_MVG) {
                        navigate(new MVG(MainActivity.this, ImageViewMVG));
                    } else if (pair.second == MenuAdapter.ROW_SUNREWARDS_POINTS) {
                        navigate(new SunRewardsPoints(MainActivity.this, (User)MySession.get().getUser()));
                    } else if (pair.second == MenuAdapter.ROW_PLAYTECH) {
                        Intent intent = new Intent(this, PlaytechActivity.class);
                        startActivityForResult(intent, Constants.PLAYTECH_ACTIVITY_REQUEST);
                    } else {
                        navigate(new Home(MainActivity.this, pair.second));
                    }

                    adapter.notifyDataSetChanged();

                }

        );

        loadURL(String.format("%s/home-v2/", Config.HOST));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d("TAG", "-- activity result. request: " + requestCode + ", result: " + resultCode);

        if (requestCode == Constants.PLAYTECH_ACTIVITY_REQUEST) {

            Log.d("TAG", "-- playtech done");
            navigate(new Home(this, 0));

        }
    }

    private boolean didFinish = false;

    private void AppLogout() {

        MySession.get().clear(this);
        finish();

        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
    }

    private void Search(String query) {

        navigate(new Search(MainActivity.this, query));

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void profile() {
        runOnUiThread(() -> {
            User user = (User)MySession.get().getUser();
            String js = String.format("Profile(\"%s\", \"%s\", \"%s\", \"%s\", \"%s\")", user.rut, user.first_name, user.last_name, user.email, user.birthday);
            callJS(js);
            ImageViewMVG.setVisibility(View.VISIBLE);

        });

    }

    public void collapseToolbar() {
        runOnUiThread(() -> appBarLayout.setExpanded(false, true));

    }

    private void updateNotificationsUI() {

        long count = Realm.getDefaultInstance().where(MyNotification.class).equalTo("read", false).count();
        Log.d("TAG", String.format("-- updating UI. count: %d", count));
        if (count > 0) {
            textViewNotifications.setVisibility(View.VISIBLE);
            textViewNotifications.setText(String.valueOf(count));
        } else {
            textViewNotifications.setVisibility(View.INVISIBLE);
        }

    }

    private void loadURL(String url) {
        drawer.closeDrawer(GravityCompat.START);
        Log.d("TAG", "-- url. " + url);
        webView.loadUrl(url);

    }

    public void callJS(String js) {
        loadURL(String.format("javascript:%s", js));
        ImageViewMVG.setVisibility(View.INVISIBLE);

    }

    /* UI */
    @OnClick(R.id.btn_settings)
    public void onClickSettings(View view) {
        drawer.closeDrawer(GravityCompat.START);
        String js = String.format("go_settings(%s)", MySession.get().getUser().isGuest() ? "true" : "false");
        Log.d("TAG", "--js " + js);
        callJS(js);
    }

    @OnClick(R.id.calendar)
    public void onClickCalendar(View view) {

        Log.d("TAG", "-- click calendar");

        navigate(new Calendar(this));

    }

    @OnClick(R.id.notifications)
    public void onClickViewNotifications() {
        Log.d("TAG", "-- clicked");

    }

    @OnClick(R.id.btn_notifications)
    public void onClickNotifications(View view) {
        Log.d("TAG", "-- notifications");
        if (viewNotifications.getVisibility() == View.GONE) {

            final NotificationsAdapter adapter = new NotificationsAdapter(this);

            if (adapter.getCount() > 0) {
                textViewNotificationsEmpty.setVisibility(View.GONE);
                viewNotificationsWrapper.setVisibility(View.VISIBLE);
            } else {
                textViewNotificationsEmpty.setVisibility(View.VISIBLE);
                viewNotificationsWrapper.setVisibility(View.GONE);
            }

            listView.setAdapter(adapter);
            listView.setOnItemClickListener((adapterView, view1, position, l) -> {
                MyNotification notification = adapter.getItem(position);
                Log.d("TAG", "clicked. " + notification.getPostID());
                callJS(String.format("load_event(%d, \"%s\")", notification.getPostID(), notification.getImage()));
                viewNotifications.setVisibility(View.GONE);
            });

            viewNotifications.setVisibility(View.VISIBLE);
            viewNotifications.setOnClickListener(v -> {
                if (viewNotifications.getVisibility() == View.VISIBLE) {
                    viewNotifications.setVisibility(View.GONE);
                }
            });
        } else if (viewNotifications.getVisibility() == View.VISIBLE) {
            viewNotifications.setVisibility(View.GONE);
        }
    }

    public void onClick(View view) {

        drawer.closeDrawer(GravityCompat.START);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.d("TAG", "-- granted");

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    Log.d("TAG", "-- denied");

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (navigateBack()) {

            } else {
                moveTaskToBack(true);
            }
            //webView.loadUrl(webView.);

            //super.onBackPressed();

        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /* Activity lifecycle */
    protected void onStart() {
        ((MonticelloApplication) getApplication()).getAPIClient().connect();
        super.onStart();
    }

    protected void onStop() {
        ((MonticelloApplication) getApplication()).getAPIClient().disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Log.d("TAG", "-- on connected");

        MonticelloApplication application = (MonticelloApplication) getApplication();

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(application.getAPIClient(),
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates states = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    MainActivity.this,
                                    100);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.

                        break;
                }
            }
        });

    }

    private NavigationStack navigationStack = new NavigationStack();
    private Command lastCommand;

    private boolean navigateBack() {
        if (navigationStack.empty())
            return false;

        lastCommand = navigationStack.pop();
        lastCommand.execute();

        return true;

    }

    private void navigate(Command command) {
        if (lastCommand != null)
            navigationStack.push(lastCommand);

        lastCommand = command;
        command.execute();

    }

    private void loadCategory(int category) {
        Command command = new Home(this, category);
        navigate(command);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private class MyNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {

        @Override
        public void notificationReceived(OSNotification osNotification) {

            Realm realm = Realm.getDefaultInstance();

//                        Log.d("TAG", "-- payload. " + receivedResult.payload.additionalData.toString());


            try {
                JSONObject data = osNotification.toJSONObject().getJSONObject("payload").getJSONObject("additionalData");

                Log.d("TAG", "-- data");
                Log.d("TAG", data.toString());


                final int postID = data.getInt("postID");
                final String title = data.getString("title");
                final String image = data.getString("image");
                // final String encodedImage = data.getString("encoded-image");

                // Use them like regular java objects
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        MyNotification myNotification = realm.createObject(MyNotification.class);
                        myNotification.setCreatedAt(System.currentTimeMillis());
                        myNotification.setPostID(postID);
                        myNotification.setTitle(title);
                        myNotification.setImage(image);
                    }
                });

                Intent intent = new Intent();
                intent.setAction("cl.magnesia.monticello.notifications");
                sendBroadcast(intent);

            } catch (JSONException exception) {
                exception.printStackTrace();
            }


        }
    }

    private class MyNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {

            OSNotification notification = result.notification;
            try {
                Log.d("TAG", "-- result");
                Log.d("TAG", result.notification.toJSONObject().toString());

                int postID = result.notification.payload.additionalData.getInt("postID");

                SharedPreferences.Editor editor = getSharedPreferences(
                        Config.PACKAGE_NAME, Context.MODE_PRIVATE).edit();
                editor.putInt(Config.KEY_OPENED_FROM_NOTIFICATION, postID);
                editor.commit();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent();
                        intent.setAction(Config.KEY_OPENED_FROM_NOTIFICATION);
                        sendBroadcast(intent);
                    }
                }, 1000);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent();
                        intent.setAction(Config.KEY_OPENED_FROM_NOTIFICATION);
                        sendBroadcast(intent);
                    }
                }, 5000);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent();
                        intent.setAction(Config.KEY_OPENED_FROM_NOTIFICATION);
                        sendBroadcast(intent);
                    }
                }, 10000);

                Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
                startActivity(intent);


            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }

}
