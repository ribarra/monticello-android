package cl.sunmonticello.activities;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cl.sunmonticello.Config;
import cl.sunmonticello.R;
import cl.sunmonticello.external.MixpanelHelper;

public class PlaytechActivity extends AppCompatActivity {

    @BindView(R.id.playtech_webview)
    WebView webView;

    /* OFFLINE */
    @BindView(R.id.layout_offline)
    View layoutOffline;

    private boolean loadSuccessfully = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_playtech);

        ButterKnife.bind(this);

        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);

        // webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webView.getSettings().setJavaScriptEnabled(true);
        //webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setAppCachePath(getApplicationContext().getFilesDir().getAbsolutePath() + "/cache");

        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setDatabasePath(getApplicationContext().getFilesDir().getAbsolutePath() + "/databases");


        webView.setWebChromeClient(new WebChromeClient(){

            @Override
            public void onProgressChanged(WebView view, int progress) {

                //Log.d("TAG", "progress. " + progress);

            }
        });
        webView.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                Log.d("TAG", "-- on page started. " + url);
                loadSuccessfully = true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {

                Log.d("TAG", "-- on page finished. " + loadSuccessfully);

                if (loadSuccessfully) {
                    webView.setVisibility(View.VISIBLE);
                    layoutOffline.setVisibility(View.GONE);
                }
                else {
                    onOffline();
                }

            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

                Log.d("TAG", "-- on error");

                loadSuccessfully = false;

            }

        });

        load();

    }

    @OnClick(R.id.layout_offline)
    public void onClickOffline() {
        layoutOffline.setVisibility(View.GONE);
        load();
    }

    private void onOffline() {
        webView.setVisibility(View.GONE);
        layoutOffline.setVisibility(View.VISIBLE);

        MixpanelHelper.get().track("WELCOME. offline");
    }

    private void callJS(String code) {
        Log.d("TAG", "--js. " + code);
        webView.loadUrl("javascript:" + code);
    }

    private void load() {

        Log.d("TAG", "-- load");
        webView.loadUrl("about:blank");
        new Handler().postDelayed(() -> webView.loadUrl(Config.PLAYTECH_URL), 500);


    }

    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Estás seguro de salir de Juega en Línea?")
                .setCancelable(false)
                .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PlaytechActivity.this.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
