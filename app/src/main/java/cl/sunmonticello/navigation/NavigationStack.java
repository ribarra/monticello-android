package cl.sunmonticello.navigation;

import java.util.Stack;

import cl.sunmonticello.navigation.commands.Command;

/**
 * Created by ribarra on 04/01/17.
 */

public class NavigationStack {

    private Stack<Command> stack = new Stack<>();

    public boolean empty()
    {
        return stack.empty();
    }

    public void push(Command command) {
        stack.push(command);

    }

    public Command peek()
    {
        return stack.peek();
    }

    public Command pop()
    {
        return stack.pop();
    }


}
