package cl.sunmonticello.navigation.commands;

import cl.sunmonticello.activities.WebViewActivity;

/**
 * Created by ribarra on 04/01/17.
 */

public class HomeFavorites implements Command {

    private WebViewActivity activity;

    public HomeFavorites(WebViewActivity activity)
    {
        this.activity = activity;
    }

    public void execute()
    {
        activity.callJS("load_favorites()");
    }

}
