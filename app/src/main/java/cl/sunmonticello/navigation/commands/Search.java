package cl.sunmonticello.navigation.commands;

import cl.sunmonticello.activities.WebViewActivity;

/**
 * Created by ribarra on 04/01/17.
 */

public class Search implements Command {

    private WebViewActivity activity;
    private String query;

    public Search(WebViewActivity activity, String query) {

        this.activity = activity;
        this.query = query;

    }

    @Override
    public void execute() {
        activity.callJS(String.format("load_search(\"%s\")", query));
    }
}
