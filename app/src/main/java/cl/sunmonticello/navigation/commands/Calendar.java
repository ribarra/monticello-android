package cl.sunmonticello.navigation.commands;

import cl.sunmonticello.activities.WebViewActivity;

/**
 * Created by ribarra on 04/01/17.
 */

public class Calendar implements Command {

    private WebViewActivity activity;

    public Calendar(WebViewActivity activity)
    {
        this.activity = activity;
    }

    @Override
    public void execute() {
            activity.callJS("go_calendar()");
    }
}
