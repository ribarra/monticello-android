package cl.sunmonticello.navigation.commands;

import cl.sunmonticello.activities.WebViewActivity;

/**
 * Created by ribarra on 04/01/17.
 */

public class Profile implements Command {

    private WebViewActivity activity;

    public Profile(WebViewActivity activity)
    {
        this.activity = activity;
    }

    public void execute()
    {
        activity.callJS("go_profile()");
    }

}
