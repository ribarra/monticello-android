package cl.sunmonticello.navigation.commands;

import cl.sunmonticello.activities.WebViewActivity;
import cl.sunmonticello.models.User;

/**
 * Created by ribarra on 04/01/19.
 */

public class SunRewardsPoints implements Command {

    private WebViewActivity activity;
    private User user;

    public SunRewardsPoints(WebViewActivity activity, User user)
    {
        this.activity = activity;
        this.user = user;
    }

    public void execute()
    {
        String js = String.format("go_points(%d, \"%s\")", user.getPoints(), user.getExpiryDate());
        activity.callJS(js);
    }

}
