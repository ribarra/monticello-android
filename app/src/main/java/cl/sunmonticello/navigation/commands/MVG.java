package cl.sunmonticello.navigation.commands;

import android.view.View;
import android.widget.ImageView;

import cl.sunmonticello.activities.WebViewActivity;

/**
 * Created by ribarra on 04/01/17.
 */

public class MVG implements Command {

    private WebViewActivity activity;
    private ImageView imageViewMVG;

    public MVG(WebViewActivity activity, ImageView imageViewMVG)
    {
        this.activity = activity;
        this.imageViewMVG = imageViewMVG;
    }

    public void execute()
    {
        activity.callJS("go_mvg()");
        imageViewMVG.setVisibility(View.VISIBLE);
    }

}
