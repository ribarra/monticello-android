package cl.sunmonticello.navigation.commands;

/**
 * Created by ribarra on 04/01/17.
 */

public interface Command {

    void execute();

}
