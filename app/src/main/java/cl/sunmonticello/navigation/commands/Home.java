package cl.sunmonticello.navigation.commands;

import cl.sunmonticello.activities.WebViewActivity;

/**
 * Created by ribarra on 04/01/17.
 */

public class Home implements Command {

    private WebViewActivity activity;
    private int category;

    public Home(WebViewActivity activity, int category)
    {
        this.activity = activity;
        this.category = category;
    }

    public void execute()
    {
        if (category > 0) {
            activity.callJS(String.format("load_home(%d)", category));
        } else {
            activity.callJS("load_home()");
        }
    }

}
