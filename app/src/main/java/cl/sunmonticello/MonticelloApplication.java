
package cl.sunmonticello;

import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import cl.sunmonticello.db.models.MyNotification;
import cl.sunmonticello.external.MixpanelHelper;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ribarra on 11/21/16.
 */


public class MonticelloApplication extends Application implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient googleAPIClient;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        // Create an instance of GoogleAPIClient.
        if (googleAPIClient == null) {
            googleAPIClient = new GoogleApiClient.Builder(this)
                    // .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();


        }

        // initializes
        MixpanelHelper.get().setup(this, Config.MIXPANEL_TOKEN);

        // Initialize Realm

        Realm.init(MonticelloApplication.this);
        Realm realm = Realm.getDefaultInstance();

        Log.d("TAG", "-- before");

        // Query Realm for all dogs younger than 2 years old
        final RealmResults<MyNotification> notifs = realm.where(MyNotification.class).findAll();
        Log.d("TAG", "-- count. " + notifs.size());

        for (int i = 0; i < notifs.size(); i++) {
            Log.d("TAG", "-- i. " + notifs.get(i).getTitle());
        }

        Log.d("TAG", "-- after");




    }

    public GoogleApiClient getAPIClient() {
        return googleAPIClient;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //return;
        }

        //Location location = LocationServices.FusedLocationApi.getLastLocation(
//                googleAPIClient);
        //Log.d("TAG", "-- location. " + location.getLatitude() +  ", " + location.getLongitude());
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
